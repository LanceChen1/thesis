function [n_g]=normals_edges(normals, d_im, dimensions, x_vals, y_vals)
    rows = dimensions(1);
    cols = dimensions(2);
    len = rows*cols;
    
    n_x = normals(:, :, 1);
    n_y = normals(:, :, 2);
    n_z = normals(:, :, 3);
    
    % sobel filters
    g_x = [-1 0 1; -2 0 2; -1 0 1];
    g_y = [-1 -2 -1; 0 0 0; 1 2 1];
    
    % grad in x direction
    x_x_grad = conv2(n_x, g_x, 'same');
    y_x_grad = conv2(n_y, g_x, 'same');
    z_x_grad = conv2(n_z, g_x, 'same');
    %x_grad = sqrt(x_x_grad.^2 + y_x_grad.^2 + z_x_grad.^2);
    
    % grad in y direction
    x_y_grad = conv2(n_x, g_y, 'same');
    y_y_grad = conv2(n_y, g_y, 'same');
    z_y_grad = conv2(n_z, g_y, 'same');
    %y_grad = sqrt(x_y_grad.^2 + y_y_grad.^2 + z_y_grad.^2);
    
    % total grad
    n_g = sqrt(x_x_grad.^2 + y_x_grad.^2 + z_x_grad.^2 + x_y_grad.^2 + y_y_grad.^2 + z_y_grad.^2);
    
    % final noise reduction filter
    n_g = medfilt2(n_g);
    %n_g = ordfilt2(n_g, 1, ones(3,3));
    
    % laplacian filter
%     l = [0 1 0; 1 -4 1; 0 1 0];
%     x_lap = conv2(n_x, l, 'same');
%     y_lap = conv2(n_y, l, 'same');
%     z_lap = conv2(n_z, l, 'same');
%     n_lap = sqrt(x_lap.^2 + y_lap.^2 + z_lap.^2);
%     
%     % final noise reduction filter
%     n_lap = medfilt2(n_lap);
    %n_lap = ordfilt2(n_lap, 1, ones(3,3));
    
%     depth_vals = reshape(d_im', 1, len);
    %non_zero_indx = find(depth_vals);
%     zero_indx = find(depth_vals == 0);
%     
%     n_g_vals = reshape(n_g', 1, len);
%     n_g_vals(zero_indx) = 0;
    %n_g_non_zero_indx = find(n_g_vals);
%     %plot_indx = union(n_g_non_zero_indx, non_zero_indx);
%     n_lap_vals = reshape(n_lap', 1, len);
%     n_lap_vals(zero_indx) = 0;
    
%     figure();
%     n_g_axes = subplot(1,2,1);
%     scatter3(n_g_axes, n_g_vals(non_zero_indx), x_vals(non_zero_indx), y_vals(non_zero_indx), 1, 'filled');
%     set(n_g_axes, 'YDir', 'reverse', 'ZDir', 'reverse');
%     xlabel(n_g_axes, 'z');
%     ylabel(n_g_axes, 'x');
%     zlabel(n_g_axes, 'y');
%     title('Normals gradients');
%     
%     n_g_colour = subplot(1,1,1);
%     scatter(n_g_colour, x_vals, y_vals, 1, n_g_vals);
%     set(n_g_colour, 'YDir', 'reverse');
%     title('Sobel colour plot');
%     
%     figure();
%     n_lap_axes = subplot(1,2,1);
%     scatter3(n_lap_axes, n_lap_vals(non_zero_indx), x_vals(non_zero_indx), y_vals(non_zero_indx), 1, 'filled');
%     set(n_lap_axes, 'YDir', 'reverse', 'ZDir', 'reverse');
%     xlabel(n_lap_axes, 'z');
%     ylabel(n_lap_axes, 'x');
%     zlabel(n_lap_axes, 'y');
%     title('Normals laplacian');
%      
%     n_lap_colour = subplot(1,1,1);
%     scatter(n_lap_colour, x_vals, y_vals, 1, n_lap_vals);
%     set(n_lap_colour, 'YDir', 'reverse');
%     title('Laplacian colour plot');
end