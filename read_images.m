% read_images.m
% because python on a vm is too slow

function [c_im, d_im, dimensions, x_vals, y_vals, depth_vals]  = read_images(image_name)

    %image_name = 'test57.png';
    c_im = imread(char(strcat('../OSD-0.2-depth/image_color/', image_name,'.png')));
    d_im = imread(char(strcat('../OSD-0.2-depth/disparity/', image_name,'.png')));

%     figure();
%     imshow(c_im);
%     figure();
%     depth_axes = subplot(1,1,1);

    %d_im = d_im(53:end-4, 1:568);
    d_im = d_im(160:end-4, 130:568);
    dimensions = size(d_im);
    rows = dimensions(1);
    cols = dimensions(2);
    len = rows*cols;

    x = [1:cols];
    y = [1:rows];
    x_vals = repmat(x, 1, rows);
    y_vals = repelem(y, cols);

    depth_vals = reshape(d_im', 1, len);

    % matlab to image axis
    % z -> y, y -> x, x -> z 
%     d = scatter3(depth_axes, depth_vals, x_vals, y_vals, 1, 'filled');%, ...
%     %'YDir', 'reverse', 'ZDir', 'reverse');
%     set(depth_axes, 'YDir', 'reverse', 'ZDir', 'reverse');
%     xlabel(depth_axes, 'z');
%     ylabel(depth_axes, 'x');
%     zlabel(depth_axes, 'y');

end

