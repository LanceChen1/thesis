function [normals]=calculate_normals(d_im, dimensions, x_vals, y_vals)
    rows = dimensions(1);
    cols = dimensions(2);
    len = rows*cols;
    
%     depth_vals = reshape(d_im', 1, len);
%     non_zero_indx = find(depth_vals);
    
    % sobel filters
    g_x = [-1 0 1; -2 0 2; -1 0 1];
    g_y = [-1 -2 -1; 0 0 0; 1 2 1];
    
    f_x = conv2(d_im, g_x, 'same');
    f_y = conv2(d_im, g_y, 'same');
    f_x = imgaussfilt(f_x, 'FilterSize', [5 5]);
    f_y = imgaussfilt(f_y, 'FilterSize', [5 5]);
    f_z = -1 * ones(rows, cols);
    
    norm = sqrt(f_x.^2 + f_y.^2 + f_z.^2);
    n_x = f_x./norm;
    n_y = f_y./norm;
    n_z = f_z./norm;
    normals = cat(3, n_x, n_y, n_z);
    %normals = cat(3, f_x, f_y, f_z);
    
%     n_x_vals = reshape(n_x', 1, len);
%     n_y_vals = reshape(n_y', 1, len);
%     n_z_vals = -1 * ones(1, len);
%     
%     figure();
%     n_ax = subplot(1,1,1);
%     %scatter3(n_ax, depth_vals(non_zero_indx), x_vals(non_zero_indx), y_vals(non_zero_indx), 0.5, 'filled');
%     %hold on;
%     quiver3(n_ax, depth_vals(non_zero_indx), x_vals(non_zero_indx), y_vals(non_zero_indx), ...
%         n_z_vals(non_zero_indx), n_x_vals(non_zero_indx), n_y_vals(non_zero_indx), ...
%         'AutoScaleFactor', 4, 'LineWidth', 0.1, 'Marker', 'o', 'MarkerSize', 0.8, 'MarkerEdgeColor', 'none', 'MarkerFaceColor', 'g');
%     set(n_ax, 'YDir', 'reverse', 'ZDir', 'reverse');
%     xlabel(n_ax, 'z'); 
%     ylabel(n_ax, 'x');
%     zlabel(n_ax, 'y');
    
end