function [n_sobel, thresholded, n_sobel_binary, num_groups]=detect_edges(d_im, dimensions, x_vals, y_vals)
    rows = dimensions(1);
    cols = dimensions(2);
    len = rows*cols;
    
    noise_removed = medfilt2(d_im);
    %noise_removed = imgaussfilt(d_im);
    
%     dnr_vals = reshape(noise_removed', 1, len); 
%     non_zero_indx = find(dnr_vals);
    
%     figure();
%     dnr_axes = subplot(1,2,1);
%     scatter3(dnr_axes, dnr_vals, x_vals, y_vals, 1, 'filled');
%     set(dnr_axes, 'YDir', 'reverse', 'ZDir', 'reverse');
%     xlabel(dnr_axes, 'z');
%     ylabel(dnr_axes, 'x');
%     zlabel(dnr_axes, 'y');
%     title('Noise Removed, 3x3');
%     
%     non_zero_axes = subplot(1,2,2);
%     scatter3(non_zero_axes, dnr_vals(non_zero_indx), x_vals(non_zero_indx), y_vals(non_zero_indx), 1, 'filled');
%     set(non_zero_axes, 'YDir', 'reverse', 'ZDir', 'reverse');
%     xlabel(non_zero_axes, 'z');
%     ylabel(non_zero_axes, 'x');
%     zlabel(non_zero_axes, 'y');
%     title('No zeroes');

%     figure();
%     gs_axes = subplot(1,1,1);
%     [surf_x, surf_y] = meshgrid(1:cols, 1:rows);
%     surf(gs_axes, surf_x, surf_y, g);
%     set(gs_axes, 'XDir', 'reverse');
%     %xlabel(g_axes, 'z');
%     %ylabel(g_axes, 'x');
%     %zlabel(g_axes, 'y');
%     title('Gradient surface');

    %laplacian_edges(noise_removed, dimensions, x_vals, y_vals);
    %gradient_edges(noise_removed, dimensions, x_vals, y_vals);
    [normals] = calculate_normals(noise_removed, dimensions, x_vals, y_vals);
    [n_sobel] = normals_edges(normals, noise_removed, dimensions, x_vals, y_vals);
    [thresholded, n_sobel_binary, num_groups] = simple_binarise_normals_edges(n_sobel, d_im, dimensions, x_vals, y_vals);
    %n_laplacian_binary = simple_binarise_normals_edges(n_laplacian, d_im, dimensions, x_vals, y_vals);
    
%     figure();
%     imshow(n_sobel_binary);
%     title('sobel binary');
%     figure();
%     imshow(n_laplacian_binary);
%     title('laplacian binary');

end