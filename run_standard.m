clear;
index = 111;
figure('visible', 'off');
for j=65:-1:0
    image_name = strcat('test', string(j));
    [c_im, d_im, dimensions, x_vals, y_vals, depth_vals]  = read_images(image_name);
    [n_sobel, thresholded, n_sobel_binary, num_groups]=detect_edges(d_im, dimensions, x_vals, y_vals);
    
    len = dimensions(1) * dimensions(2);
    
    imshow(thresholded);
    saveas(gcf, char(strcat('../results/standard/thresholded/', image_name, '_thresholded.png')));
    
    imshow(n_sobel_binary);
    saveas(gcf, char(strcat('../results/standard/final/', image_name, '_final.png')));
    
    non_zero_indx = find(depth_vals);
    zero_indx = find(depth_vals == 0);
    n_g_vals = reshape(n_sobel', 1, len);
    n_g_vals(zero_indx) = 0;
    
    n_g_colour = subplot(1,1,1);
    scatter(n_g_colour, x_vals, y_vals, 1, n_g_vals);
    set(n_g_colour, 'YDir', 'reverse');
    set(n_g_colour, 'visible', 'off');
    saveas(gcf, char(strcat('../results/standard/gradients/', image_name, '_gradients.png')));
     
    total_edges_thresholded = length(find(thresholded == 0));
    edge_percentage_thresholded = (total_edges_thresholded / len) * 100;
    
    total_edges_final = length(find(n_sobel_binary == 0));
    edge_percentage_final = (total_edges_final / len) * 100;
    
    statistics(index).image_name = image_name;
    statistics(index).edge_percentage_thresholded = edge_percentage_thresholded;
    statistics(index).edge_percentage_final = edge_percentage_final;
    statistics(index).num_groups = num_groups;
    index = index - 1;
end

for i=44:-1:0
    image_name = strcat('learn', string(i));
    [c_im, d_im, dimensions, x_vals, y_vals, depth_vals]  = read_images(image_name);
    [n_sobel, thresholded, n_sobel_binary, num_groups]=detect_edges(d_im, dimensions, x_vals, y_vals);
    
    len = dimensions(1) * dimensions(2);
    
    imshow(thresholded);
    saveas(gcf, char(strcat('../results/standard/thresholded/', image_name, '_thresholded.png')));
    
    imshow(n_sobel_binary);
    saveas(gcf, char(strcat('../results/standard/final/', image_name, '_final.png')));
    
    non_zero_indx = find(depth_vals);
    zero_indx = find(depth_vals == 0);
    n_g_vals = reshape(n_sobel', 1, len);
    n_g_vals(zero_indx) = 0;
    
    n_g_colour = subplot(1,1,1);
    scatter(n_g_colour, x_vals, y_vals, 1, n_g_vals);
    set(n_g_colour, 'YDir', 'reverse');
    set(n_g_colour, 'visible', 'off');
    saveas(gcf, char(strcat('../results/standard/gradients/', image_name, '_gradients.png')));
     
    total_edges_thresholded = length(find(thresholded == 0));
    edge_percentage_thresholded = (total_edges_thresholded / len) * 100;
    
    total_edges_final = length(find(n_sobel_binary == 0));
    edge_percentage_final = (total_edges_final / len) * 100;
    
    statistics(index).image_name = image_name;
    statistics(index).edge_percentage_thresholded = edge_percentage_thresholded;
    statistics(index).edge_percentage_final = edge_percentage_final;
    statistics(index).num_groups = num_groups;
    index = index - 1;
end

save('../results/standard/statistics', 'statistics');