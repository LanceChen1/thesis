function [thresholded, result, num_groups]=simple_binarise_normals_edges(normals_edges, d_im, dimensions, x_vals, y_vals)
    rows = dimensions(1);
    cols = dimensions(2);
    len = rows*cols;
    
    vals = normals_edges(d_im ~= 0);
  
    %max_val = max(vals);
    %min_val = min(vals);
    %threshold = (max_val + min_val) / 2;
    threshold = median(vals);
    %threshold = (max_val + med) /2;
    
    binarised = ones(rows, cols);
    binarised(normals_edges >= threshold) = 0;
    binarised(d_im == 0) = 0;
    thresholded = medfilt2(binarised);
    %binarised = ordfilt2(binarised, 9, ones(3,3));
    
    [result, num_groups] = connected_components_filter(thresholded, dimensions, x_vals, y_vals);
end