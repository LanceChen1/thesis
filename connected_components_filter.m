function [filtered_binary_image, num_groups]=connected_components_filter(binary_image, dimensions, x_vals, y_vals)
    rows = dimensions(1);
    cols = dimensions(2);
    len = rows*cols;

    group = zeros(rows, cols);
    num_groups = 0;

    inv_binary_image = ~binary_image;
    conn_degree = colfilt(inv_binary_image, [3 3], 'sliding', @sum);
    
    for i=1:rows
        for j=1:cols
            if group(i,j) == 0
                if inv_binary_image(i,j)
                    num_groups = num_groups + 1;
                    label_pixel(i,j);
                else
                    group(i,j) = -1;
                end
            end
        end
    end

    %plot();
    group_sizes = zeros(num_groups, 1);
    
    for k=1:num_groups
        group_sizes(k,1) = length(find(group == k));
    end
    
%     num_groups
%     group_sizes
%     figure();
%     edges = [0 100 200 300 400 500 600 700 800 900 1000 max(group_sizes)];
%     histogram(group_sizes, edges);
    min_size = 10000;
    noise_groups = find(group_sizes < min_size);
    group_plot = group;
    group_plot(ismember(group_plot, noise_groups)) = -1;
    
    filtered_binary_image = zeros(rows, cols);
    filtered_binary_image(group_plot == -1) = 1;
%     filtered_binary_image = ~filtered_binary_image;
%     
%     filtered_binary_image = bwmorph(filtered_binary_image, 'spur', 1);
%     filtered_binary_image = ~filtered_binary_image;

    %plot();
    
    function []=plot()
        %group_vals = reshape(group_plot', 1, len);
        figure();
        original = subplot(1,2,1);
        imshow(binary_image);
        %scatter(original, x_vals, y_vals, 1, 'filled', 'MarkerFaceColor', 'black');
        set(original, 'YDir', 'reverse');
        title('original');

        %edge_indx = find(group_vals > 0);
        only_edges = subplot(1,2,2);
        %scatter(only_edges, x_vals(edge_indx), y_vals(edge_indx), 1, 'filled', 'MarkerFaceColor', 'black');
        imshow(filtered_binary_image);
        set(only_edges, 'YDir', 'reverse');
        title('filtered '+string(min_size));
    end

    function label_pixel(r, c)
        %r,c
        group(r,c) = num_groups;
        
        if conn_degree(r,c) >= 5
            r_minus = true;
            r_plus = true;
            c_minus = true;
            c_plus = true;

            if r == 1
                r_minus = false;
            elseif r == rows 
                r_plus = false;
            end

            if c == 1
                c_minus = false;
            elseif c == cols
                c_plus = false;
            end

            if c_minus
                if group(r, c-1) == 0
                   if inv_binary_image(r, c-1)
                       label_pixel(r, c-1);
                   else
                       group(r, c-1) = -1;
                   end
                end
            end

            if c_plus
                if group(r, c+1) == 0
                    if inv_binary_image(r, c+1)
                        label_pixel(r, c+1);
                    else
                        group(r, c+1) = -1;
                    end
                end
            end

            if r_minus
               if group(r-1, c) == 0
                      if inv_binary_image(r-1,c)
                          label_pixel(r-1,c);
                      else
                          group(r-1,c) = -1;
                      end
               end

               if c_minus
                   if group(r-1, c-1) == 0
                      if inv_binary_image(r-1,c-1)
                          label_pixel(r-1,c-1);
                      else
                          group(r-1,c-1) = -1;
                      end
                   end
               end
               if c_plus
                   if group(r-1, c+1) == 0
                      if inv_binary_image(r-1,c+1)
                          label_pixel(r-1,c+1);
                      else
                          group(r-1,c+1) = -1;
                      end
                   end
               end
            end

            if r_plus
               if group(r+1, c) == 0
                      if inv_binary_image(r+1,c)
                          label_pixel(r+1,c);
                      else
                          group(r+1,c) = -1;
                      end
               end

               if c_minus
                   if group(r+1, c-1) == 0
                      if inv_binary_image(r+1,c-1)
                          label_pixel(r+1,c-1);
                      else
                          group(r+1,c-1) = -1;
                      end
                   end
               end
               if c_plus
                   if group(r+1, c+1) == 0
                      if inv_binary_image(r+1,c+1)
                          label_pixel(r+1,c+1);
                      else
                          group(r+1,c+1) = -1;
                      end
                   end
               end
            end
        end
    end
    
    
end